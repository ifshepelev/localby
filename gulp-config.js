const gulpConfig = {
	mode: process.env.NODE_ENV || 'development',
	development: {
		source: 'app/',
		dest: 'public/',
		assets: 'public/assets/',
		scripts: 'public/assets/scripts/',
		styles: 'public/assets/styles/',
		images: 'public/assets/images/',
		icons: 'public/assets/images/',
		entry: {
			app: [
				'./scripts/app.js',
			],
			landing: [
				'./scripts/app.js',
			],
		},
		basePath: './',
		isDebug: true,
		local: true
	},
	// stage: {
	// 	source: 'app/',
	// 	dest: '../views/',
	// 	assets: '../views/assets/',
	// 	scripts: '../views/assets/js/',
	// 	styles: '../views/assets/css/',
	// 	images: '../web/images/',
	// 	icons: '../web/images/',
	// 	entry: {
	// 		app: './scripts/app.js',
	// 	},
	// 	basePath: '../',
	// 	isDebug: true,
	// 	local: false
	// },
	// production: {
	// 	source: 'app/',
	// 	dest: '../views/',
	// 	assets: '../views/assets/',
	// 	scripts: '../views/assets/js/',
	// 	styles: '../views/assets/css/',
	// 	images: '../web/images/',
	// 	icons: '../web/images/',
	// 	entry: {
	// 		app: './scripts/app.js',
	// 	},
	// 	basePath: '/',
	// 	isDebug: false,
	// 	local: false
	// }
};

export default gulpConfig;
