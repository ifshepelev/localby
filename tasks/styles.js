import gulp from 'gulp';
import stylus from 'gulp-stylus';
import debug from 'gulp-debug';
import rupture from 'rupture';
import gulpIf from 'gulp-if';
import cssNano from 'gulp-cssnano';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import postcss from 'gulp-postcss';
import gulpConfig from '../gulp-config';

const mode = gulpConfig.mode;
const isDebug = gulpConfig[mode].isDebug;

const entries = Object.keys(gulpConfig[mode].entry);
const sources = [];

const data = {path: gulpConfig[mode].basePath};

entries.forEach(elem => {
	sources.push(`${gulpConfig[mode].source}styles/${elem}.styl`);
});

export function styles() {
	return gulp.src(sources)
		.pipe(debug({title: 'stylus'}))
		.pipe(gulpIf(isDebug, sourcemaps.init()))
		.pipe(stylus({
			rawDefine: {data: data},
			use: [rupture()],
			'include css': true
		}))
		.pipe(postcss([ require('postcss-preset-env') ]))
		.pipe(gulpIf(!isDebug, cssNano()))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulpIf(isDebug, sourcemaps.write()))
		.pipe(gulp.dest(gulpConfig[mode].styles));
}

export function stylesWatch() {
	return gulp.watch([`${gulpConfig[mode].source}components/**/*.styl`], gulp.series('styles'));
}
