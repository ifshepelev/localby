import gulp from 'gulp';
import gulpConfig from '../gulp-config';

const mode = gulpConfig.mode;

export function assets() {
	return gulp.src(`${gulpConfig[mode].source}assets/**/*`, {base: gulpConfig[mode].source})
		.pipe(gulp.dest(gulpConfig[mode].dest));
}
