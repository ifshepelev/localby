import gulp from 'gulp';
import Browser from 'browser-sync';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';

import mainWebpackConfig from '../webpack-dev-middleware.config.babel';

const browser = Browser.create();
const bundler = webpack(mainWebpackConfig());

export default function server() {
  browser.init({
    server: {
      baseDir: './public/',
      port: 3000,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials-Origin': true,
      },
    },
    startPath: './index.html',
  });

  gulp.watch(['public/assets/styles/*.css', 'public/assets/scripts/*.js', 'public/*.html']).on('change', () => browser.reload());
}
