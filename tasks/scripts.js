import PluginError from 'plugin-error';
import log from 'fancy-log';
import webpack from 'webpack';
import mainWebpackConfig from '../webpack.config.babel';
import gulpConfig from '../gulp-config';

const mode = gulpConfig.mode;
const isDebug = gulpConfig[mode].isDebug;
/**
 * Запуск webpack
 * @param callback - коллбек, resolve gulp task
 * @param watch
 * @param debug
 */
function runWebpack(callback, watch, debug) {
	return webpack(mainWebpackConfig(watch, debug),
		function(err, stats) {
			if (err) {
				throw new PluginError('webpack', err);
			}
			log('[webpack]', stats.toString({
				errors: true,
				warnings: true,
				colors: true,
				children: false,
				chunks: false,
				modules: true,
				timings: true
			}));
			callback();
		});
}

export function scripts(callback) {
	return runWebpack(callback, false, isDebug);
}

export function scriptsWatch(callback) {
	return runWebpack(callback, true, isDebug);
}