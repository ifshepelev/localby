import gulp from 'gulp';
import imagemin from'gulp-imagemin';
import gulpConfig from '../gulp-config';

const mode = gulpConfig.mode;

export function images() {
	return gulp.src([`${gulpConfig[mode].source}images/*`])
		.pipe(imagemin())
		.pipe(gulp.dest(gulpConfig[mode].images));
}
