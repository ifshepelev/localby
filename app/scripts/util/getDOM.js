window.getDom = (self) => {
  Object.keys(self.options.dom).forEach(item => {
    const keyName = `${item}`;
    self[keyName] = document.querySelectorAll(self.options.dom[item]);
    if (self[keyName].length === 1) {
      self[keyName] = self[keyName][0];
    }
  });
};
