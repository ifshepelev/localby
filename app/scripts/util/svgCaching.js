
(function() {
	'use strict';

	const stats = require('../../../public/assets/scripts/stats.json');

	let file = `${window.location.protocol + "//" + window.location.host}/images/icons.svg`;

	if (process.env.IS_LOCAL) {
		file = './assets/images/icons.svg'
	}
	const revision = stats.hash;

	if(!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect)
		return true;

	const isLocalStorage = 'localStorage' in window && window['localStorage'] !== null;
	let request;
	let data;
	const preload = document.querySelector('.js-svg-preload');
	const insertIT = () => {
		preload.insertAdjacentHTML('afterbegin', data);
	};
	const insert = () => {
		if( document.body ) insertIT();
		else document.addEventListener('DOMContentLoaded', insertIT);
	};

	if(isLocalStorage && localStorage.getItem('inlineSVGrev') === revision) {
		data = localStorage.getItem('inlineSVGdata');
		if (data) {
			insert();
			return true;
		}
	}

	try {
		request = new XMLHttpRequest();
		request.open('GET', file, true);
		request.onload = function() {
			if(request.status >= 200 && request.status < 400) {
				data = request.responseText;
				insert();
				if( isLocalStorage ) {
					localStorage.setItem('inlineSVGdata', data);
					localStorage.setItem('inlineSVGrev', toString(revision));
				}
			}
		}
		request.send();
	}
	catch(e){}

}());