NodeList.prototype.addEventListener = function (event, callback) {
  if (NodeList.prototype.isPrototypeOf(this)) {
    this.forEach((elem) => {
      elem.addEventListener(event, callback);
    });
  }
};
