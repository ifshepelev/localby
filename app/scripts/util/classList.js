if (!Element.prototype.hasClass) {
    Element.prototype.hasClass = function (className) {
        if (Element.prototype.isPrototypeOf(this)) {
            return this.classList.contains(className.replace('.', ''));
        }
    };
}