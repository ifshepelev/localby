import smoothscroll from 'smoothscroll-polyfill';
import './util/listen';
import './util/classList';
import './util/svgCaching';
import './util/closest';
import './util/getDOM';
import Layout from '../components/layout/layout';
import Scores from '../components/scores/scores';

(() => {
  new Layout().render();
  new Scores().render();
  smoothscroll.polyfill();
})();
