import extend from 'extend';
import bowser from 'bowser';

export default class Layout {
  constructor(options) {
    const defaults = {
      el: '.page',
      dom: {},
      classes: {},
      flags: ['chrome', 'firefox', 'msie', 'msedge', 'safari', 'android', 'ios', 'mac', 'windows', 'linux', 'android', 'ios'],
    };

    this.options = extend(true, {}, defaults, options);
    this.el = document.querySelector(this.options.el);
  }

  getDom() {
    Object.keys(this.options.dom).forEach((item) => {
      const keyName = `${item}`;
      this[keyName] = document.querySelectorAll(this.options.dom[item]);
    });
  }

  setUserEnv() {
    this.options.flags.forEach((elem) => {
      if (bowser.hasOwnProperty(elem)) {
        document.documentElement.classList.add(elem);
      }
    });
  }

  render() {
    this.setUserEnv();
    if (this.el) {
      this.getDom();
    }
  }
}
