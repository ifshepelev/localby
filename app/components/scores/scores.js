import extend from 'extend';

export default class Scores {
  close;

  constructor(options) {
    const defaults = {
      el: '.js-notification',
      dom: {
        close: '.js-notification-close',
      },
    };

    this.options = extend(true, {}, defaults, options);
    this.el = document.querySelector(this.options.el);
  }

  render() {
    if (this.el) {
      window.getDom(this);

      setTimeout(() => { window.scrollTo({ left: 0, top: 1 }); }, 100);

      this.close.addEventListener('click', (e) => {
        e.preventDefault();
        const { target } = e;
        const el = target.closest(this.options.el);
        el.parentNode.removeChild(el);
      });
    }
  }
}
