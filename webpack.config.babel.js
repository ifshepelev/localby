import path from 'path';
import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import StatsPlugin from 'webpack-stats-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import gulpConfig from './gulp-config';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const StatsWriterPlugin = StatsPlugin.StatsWriterPlugin;
const mode = gulpConfig.mode;

const env = gulpConfig.mode.isDebug ? 'development' : 'production';
const entry = gulpConfig[mode].entry;
const isLocal = gulpConfig[mode].local;
const isDebug = gulpConfig[mode].isDebug;

export default function mainWebpackConfig(
	watch = false,
	debug = false,
) {
	const webpackConfig = {
		context: path.resolve(__dirname, 'app'),
		watch,
		entry,
		output: {
			publicPath: path.resolve(__dirname, 'public'),
			path: path.resolve(__dirname, `${gulpConfig[mode].scripts}`),
			filename: '[name].min.js'
		},
		devServer: {
			contentBase: './public',
			port: 3000,
		},
		mode: env,
		module: {
			rules: [
				{
					test: /\.js$/,
					loader: 'babel-loader',
					exclude: [/node_modules/],
					query: {
						plugins: ['transform-runtime', 'transform-class-properties', 'transform-object-rest-spread'],
						presets: ['babel-preset-env']
					}
				}
				// , {
				//    test: /\.js$/,
				//    exclude: [/node_modules/, /\/app\/scripts\/util\//],
				//    use: {
				//        loader: 'eslint-loader',
				//        options: {
				//            configFile: path.resolve(__dirname, '.eslintrc'),
				//            failOnWarning: false,
				//            failOnError: false,
				//        },
				//    },
				// },
			]
		},
		plugins: [
			new webpack.optimize.ModuleConcatenationPlugin(),
			new webpack.WatchIgnorePlugin([/stats.json/]),
			new StatsWriterPlugin({
				filename: 'stats.json',
				fields: ['version', /* 'warnings',  'errors',*/ 'hash' /* 'assets', 'chunks' , 'modules'*/],
				transform: data => JSON.stringify(data, null, 2)
			}),
			new CopyWebpackPlugin([
				{from: `${__dirname}/node_modules/babel-polyfill/dist/polyfill.min.js`}
			]),
			// new HtmlWebpackPlugin({
			// 	template: 'public/index.html',
			// 	filename: 'index.html'
			// }),

		],
		devtool: debug ? 'eval' : false,
		performance: {hints: false}
	};

	if (!debug) {
		webpackConfig.plugins.push(
			new UglifyJsPlugin({
				sourceMap: debug,
				uglifyOptions: {
					output: {comments: debug}
				}
			})
		);
	}

	webpackConfig.plugins.push(
		new webpack.DefinePlugin({
			'process.env': {
				IS_LOCAL: isLocal,
				IS_DEBUG: isDebug
			}
		}),
	);

	return webpackConfig;
}
