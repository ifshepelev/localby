import path from 'path';
import webpack from 'webpack';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import StatsPlugin from 'webpack-stats-plugin';
import gulpConfig from './gulp-config';

const { StatsWriterPlugin } = StatsPlugin;
const mode = 'development';
const env = 'development';

export default function mainWebpackConfig() {
  const webpackConfig = {
    context: path.resolve(__dirname, 'app'),
    entry: {
      app: [
        './scripts/app.js',
      ],
    },
    output: {
      publicPath: path.resolve(__dirname, 'public'),
      path: path.resolve(__dirname, `${gulpConfig[mode].scripts}`),
      filename: '[name].min.js',
    },
    watch: true,
    mode: env,
    devServer: {
      hot: true,
      contentBase: './public',
      port: 3000,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: [/node_modules/],
          query: {
            plugins: ['transform-runtime', 'transform-class-properties', 'transform-object-rest-spread'],
            presets: ['babel-preset-env'],
          },
        },
      ],
    },
    plugins: [
      new webpack.optimize.ModuleConcatenationPlugin(),
      new StatsWriterPlugin({
        filename: 'stats.json',
        fields: ['version', /* 'warnings',  'errors', */ 'hash'],
        transform: (data) => JSON.stringify(data, null, 2),
      }),
      new CopyWebpackPlugin([
        { from: `${__dirname}/node_modules/babel-polyfill/dist/polyfill.min.js` },
      ]),
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
    ],
    // devtool: 'eval',
    // performance: {hints: false}
  };

  webpackConfig.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        IS_LOCAL: true,
        IS_DEBUG: true,
      },
    }),
  );

  return webpackConfig;
}
