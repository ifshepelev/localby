import gulp from 'gulp';
import del from 'del';
import {styles, stylesWatch,} from './tasks/styles';
import {scripts, scriptsWatch} from './tasks/scripts';
import {assets} from './tasks/assets';
import {images} from './tasks/images';
import {icons} from './tasks/icons';
import {server} from './tasks/server';
import gulpConfig from './gulp-config';

gulp.task('styles', () => styles());
gulp.task('styles:watch', () => stylesWatch());

gulp.task('scripts', callback => scripts(callback));
gulp.task('scripts:watch', callback => scriptsWatch(callback));

gulp.task('images', () => images());
gulp.task('icons', () => icons());
gulp.task('assets', () => assets());
gulp.task('server', () => server());

gulp.task('watch', gulp.parallel('styles:watch', 'scripts:watch'));

gulp.task('clean', () => {
	return del(gulpConfig.development.assets);
});

gulp.task('build', gulp.parallel('scripts', 'styles', 'assets'));

gulp.task('start', gulp.parallel('server', 'scripts:watch', 'styles:watch'));

gulp.task('default', () => {});
